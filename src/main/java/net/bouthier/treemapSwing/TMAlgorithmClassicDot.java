package net.bouthier.treemapSwing;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;

import experimentextensions.RandomTreeMapNode;

public class TMAlgorithmClassicDot extends TMAlgorithmClassic {

	private static final Paint DOTCOLOR = Color.BLACK;
	private static final int DOTSIZE = 10;

	/**
     * Draws the node and recurses the drawing on its children.
     *
     * @param g        the graphic context
     * @param node     the node to draw
     * @param axis     the axis of separation
     * @param level    the level of deep
     */
    protected void drawNodes(Graphics2D  g,
        					 TMNodeModel node,
        					 short 		 axis,
        					 int 		 level) {
    	super.drawNodes(g, node, axis, level);
        
    	if(node.node instanceof RandomTreeMapNode && ((RandomTreeMapNode)node.node).isSearchObject()) {
    		Rectangle area = node.getArea();
    		g.setPaint(TMAlgorithmClassicDot.DOTCOLOR);
    		g.fillOval(area.x + area.width / 2 - DOTSIZE / 2, area.y + area.height / 2 - DOTSIZE / 2, DOTSIZE, DOTSIZE);
    	}
        
    }
}
